# PhenoFly Planning Tool

PhenoFly Planning Tool enhances recent sophisticated UAS and autopilot systems with an optical remote sensing workflow that respects photographic concepts. The tool can assist in selecting the right equipment for your needs, experimenting with different flight settings to test the performance of the resulting imagery, preparing the field and GCP setup, and generating a flight path that can be exported as waypoints to be uploaded to an UAS.

## Installation

### ETHZ Shiny server

To use the application on the ETHZ shiny server no installation is needed. 
Simply open the URL [https://shiny.usys.ethz.ch/PhenoFlyPlanningTool](https://shiny.usys.ethz.ch/PhenoFlyPlanningTool) in a web-browser.

  - **Advantages**: No installation needed
  - **Disadvantages**: Configuration file for your camera / drone-setup needs to be uploaded every time you start a new session
 
### Local

Using R-Studio you can create an add-hoc shiny server and run the application locally. To do so, 

  1. Download the source-code as archive ([1](https://gitlab.ethz.ch/crop_phenotyping/PhenoFlyPlanningTool/-/archive/master/PhenoFlyPlanningTool-master.zip)) or using git ([2](https://gitlab.ethz.ch/crop_phenotyping/PhenoFlyPlanningTool.git)) 
  2. Open the file `app.R` in R-Studio
  3. Start the application using the `run app` button
  
  
  - **Advantages**: You can add custom project files to the folder `sample_projects`
  - **Disadvantages**: Single user only

### Third-party or own Shiny server

You can host the application on your own Shiny server or a cloud-based server (e.g. [http://www.shinyapps.io](http://www.shinyapps.io/))
  
  - **Advantages**: You can add custom project files to the folder `sample_projects`
  - **Disadvantages**: Installation or cloud account needed

## Quick start guide

An extensive guide is available as software publication in the open-access journal plant methods (Roth, L., Hund, A. and Aasen, H. (2018). PhenoFly Planning Tool: Flight planning for high-resolution optical remote sensing with unmanned areal systems. Plant Methods, 14(1).) [https://doi.org/10.1186/s13007-018-0376-6]

As a quick start into PhenoFly, process as following:

 1. Access [https://shiny.usys.ethz.ch/PhenoFlyPlanningTool/] in a web-browser
 2. Select `001_Sony_A9_M600P.json` as predefined project and click `Load`
 3. `OK`
 4. Change the Sensor/Lens, Imaging, Mapping, GCPs and Location setting to your needs
 5. Click `Save project` to download a JSON file containing your project settings

Next time you can start using your custom JSON project file by:
 1. Access [https://shiny.usys.ethz.ch/PhenoFlyPlanningTool/] in a web-browser
 2. Select `Open project`
 3. Upload your project file
 4. `OK`
 
 


## Contributions

If you have ready-to-use project configurations for a specific sensor please feel free to send them to us (lukas.roth@usys.ethz.ch), we will validate and integrate them in the ETHZ Shiny server installation if we think it is of use for other users too.

