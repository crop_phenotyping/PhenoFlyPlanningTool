###
#  PhenoFly Planning Tool
#  Copyright (C) 2018  ETH Zürich, Lukas Roth (lukas.roth@usys.ethz.ch)
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
###


library(shinycssloaders)
library(leaflet)
library(shinyBS)

# Default values
default_exposure_value <- 14
default_max_motion_blur <- 0.04
default_iso <- 4000
default_shutter_speed <- 16000
default_flight_height <- 30
default_mapping_area_x <- 36
default_mapping_area_y <- 40
default_side_lap <- 60
default_end_lap <- 80
default_no_gcp_x <- 6
default_no_gcp_y <- 3
default_flip_camera <- FALSE
default_plot_size_x <- 1.5
default_plot_size_y <- 2
default_position_precision <- NULL
default_sensor_name <- NULL
default_d_sensor_x<- NULL
default_d_sensor_y<- NULL
default_n_pix_x<- NULL
default_n_pix_y<- NULL
default_lens_aperture<- 8
default_focal_lenght<- NULL
default_t_max<- NULL
default_ISO_max<- NULL
default_freq_max<- NULL
default_flight_max<- NULL
default_max_number_of_wp <- 99
default_gcp_rec_frequency <- 50
default_gcp_rec_number <- 2

default_position_edge1_lat <- 47.450812627526901
default_position_edge1_long <- 8.682496912397921
default_position_edge2_lat <- 47.451039942926123
default_position_edge2_long <- 8.682089627868457
default_starting_point_lat <- 47.450163
default_starting_point_long <- 8.682736

aperture_values <- c(0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.6, 1.7, 1.8, 2, 2.2, 2.4, 2.5, 2.8, 3.2, 3.4, 4, 4.5, 4.8, 5, 5.6, 6.3, 6.7, 7.1, 8, 9, 9.5, 10.1, 11.3, 12.7, 13.5, 14.3, 16, 18, 19)
names(aperture_values) <- paste0("f/", aperture_values)

ui_ <- fluidPage(
  shinyjs::useShinyjs(),
  
  titlePanel(windowTitle="PheonyFly Planning Tool",
    title=div(
              fluidRow(
                column(4, img(src="CSLogo_black_40.png"), HTML("Pheno<b>Fly Planning Tool</b>"),
                       tags$style(".shiny-file-input-progress {display: none}")),
                column(3, textOutput("project_name")),
                column(2, actionButton("show_start_modal", "Edit project", width = '100%', icon=icon("edit"))),
                tags$style(type='text/css', "#show_start_modal { width:100%; margin-top: -6px;}"),
                column(2, downloadButton("download_params", "Save project", width='100%')),
                tags$style(type='text/css', "#download_params { width:100%; margin-top: -6px;}")
              ))
    
    ),
  
  sidebarLayout(
    
    sidebarPanel(
      
      tabsetPanel(id="config_tab",
        tabPanel("Sensor/Lens",
          h4("Sensor"),
          fluidRow(
            column(12, textInput("sensor_and_lens_select", label=NA, value= default_sensor_name))
          ),
          fluidRow(
            column(6, numericInput("d_sensor_x", "Sensor size, x (mm)", width = "100%", value=default_d_sensor_x, step=0.1)),
            bsTooltip("d_sensor_x", "Physical size of sensor", placement = "bottom", trigger = "hover", options = NULL),
            column(6, numericInput("d_sensor_y", "Sensor size, y (mm)", width = "100%", value=default_d_sensor_y, step=0.1)),
            bsTooltip("d_sensor_y", "Physical size of sensor", placement = "bottom", trigger = "hover", options = NULL)
          ),
          fluidRow(
            column(6, numericInput("n_pix_x", "Number of recorded pixels, x (px)", width = "100%", value=default_n_pix_x, step=1)),
            bsTooltip("n_pix_x", "Resolution of sensor in pixel", placement = "bottom", trigger = "hover", options = NULL),
            column(6, numericInput("n_pix_y", "Number of recorded pixels, y (px)", width = "100%", value=default_n_pix_y, step=1)),
            bsTooltip("n_pix_y", "Resolution of sensor in pixel", placement = "bottom", trigger = "hover", options = NULL)
          ),
          fluidRow(
            column(6, numericInput("t_max", "Max. shutter speed (1/s)", width = "100%", value=default_t_max, step=1000)),
            bsTooltip("t_max", "Maximum shutter speed capability of the sensor (mechanical or electronic).", placement = "bottom", trigger = "hover", options = NULL),
            column(6, numericInput("ISO_max", "Max. film speed (ISO)", width = "100%", value=default_ISO_max, step=1000)),
            bsTooltip("ISO_max", "Maximum tolerable ISO setting of the sensor, determined by the maximum tolerable signal-to-noise ratio in the final product.", placement = "bottom", trigger = "hover", options = NULL)
          ),
          fluidRow(
            column(6, numericInput("freq_max", "Max. image trigger freq. (1/s)", width = "100%", value=default_freq_max)),
            bsTooltip("freq_max", "Maximum number of images the sensor can capture in one second. In most cases determined by the maxium writing speed of the storage medium.", placement = "bottom", trigger = "hover", options = NULL),
            column(6, numericInput("flight_max", "Max. flight duration (min)", width = "100%", value=default_flight_max)),
            bsTooltip("flight_max", "Maximum flight time capability of the drone that carries the sensor.", placement = "bottom", trigger = "hover", options = NULL)
          ),
          htmlOutput("d_pix_output"),
          hr(),
          h4("Lens"),
          fluidRow(
            column(6, numericInput("f", "Focal length (mm)", width = "100%", value=default_focal_lenght, step=1)),
            bsTooltip("f", "Physical focal length (not 35 mm equivalent).", placement = "bottom", trigger = "hover", options = NULL),
            column(6, selectInput("aperture", "Aperture (f-number)", width = "100%", choices = aperture_values, selected=default_lens_aperture))
          )
        ),
        
        tabPanel("Imaging",
          h4("Resolution"),
          sliderInput("flight_height", "Flight height (m)", width = "100%", value = default_flight_height, min=2, max=150),
          fluidRow(
            column(9, numericInput("ground_sampling_distance", "Ground sampling distance (mm)", width = "100%", value=NA, step=0.1)),
            column(3, checkboxInput("edit_ground_sampling_distance", "Edit"), style = "margin-top: 25px;")
          ),
          hr(),
          h4("Exposure"),
          sliderInput("exposure_value", "Exposure value (EV) (sunny day: ~14, clowded:~8)", min = 1, max = 21, step = 1, value = default_exposure_value),
          bsTooltip("exposure_value", "Value describing the illumination condition of the scene, and/or a certain shutter speed, ISO and aperture combination", placement = "bottom", trigger = "hover", options = NULL),
          hr(),
          fluidRow(
            column(6, selectizeInput("shutter_speed", "Shutter speed (s)", selected = default_shutter_speed, choices = default_shutter_speed, width = "100%")),
            column(6, numericInput("iso", "Film speed (ISO)", width = "100%", value = default_iso))
          )
          
        ),
        
        tabPanel("Mapping",
          h4("Mapping area"),
          fluidRow(
            column(6, numericInput("mapping_area_x", "Mapping area, width (m)", width = "100%", value = default_mapping_area_x, step=1)),
            bsTooltip("mapping_area_x", "Size of whole mapping area", placement = "bottom", trigger = "hover", options = NULL),
            column(6, numericInput("mapping_area_y","Mapping area, depth (m)", width = "100%", value = default_mapping_area_y, step=1)),
            bsTooltip("mapping_area_y", "Size of whole mapping area", placement = "bottom", trigger = "hover", options = NULL)
          ),
          fluidRow(
            column(6, numericInput("plot_size_x", "Single plot size, width (m)", width = "100%", value = default_plot_size_x, step=0.1)),
            bsTooltip("plot_size_x", "Size of individual experimental plots", placement = "bottom", trigger = "hover", options = NULL),
            column(6, numericInput("plot_size_y","Single plot size, depth (m)", width = "100%", value = default_plot_size_y, step=0.1)),
            bsTooltip("plot_size_y", "Size of individual experimental plots", placement = "bottom", trigger = "hover", options = NULL)
          ),
          hr(),
          h4("Flight path"),
          checkboxInput("edit_spacing", "metric"),
          fluidRow(
            column(6, numericInput("side_lap", "Side lap (%)", width = "100%", value = default_side_lap)),
            bsTooltip("side_lap", "Image overlap between flight lines", placement = "bottom", trigger = "hover", options = NULL),
            column(6, numericInput("end_lap", "End lap (%)", width = "100%", value = default_end_lap)),
            bsTooltip("end_lap", "Image overlap in flight direction", placement = "bottom", trigger = "hover", options = NULL)
          ),
          fluidRow(
            column(6, numericInput("spacing_between_flight_lines", "Side lap (m)", width = "100%", value=NA)),
            bsTooltip("spacing_between_flight_lines", "Image overlap between flight lines", placement = "bottom", trigger = "hover", options = NULL),
            column(6, numericInput("spacing_between_exposures", "End lap (m)", width = "100%", value=NA)),
            bsTooltip("spacing_between_exposures", "Image overlap in flight direction", placement = "bottom", trigger = "hover", options = NULL)
          ),
          fluidRow(
            column(6,radioButtons("flip_camera", "Camera heading", choiceNames=c("Narrow side in flight direction", "Wide side in flight direction"), choiceValues = c(FALSE, TRUE), selected = default_flip_camera)),
            column(6, numericInput("positioning_precision", "Positioning precision (m)", width = "100%", value = default_position_precision)),
            bsTooltip("positioning_precision", "Standard deviation of positioning precision of the drone for way-point flights", placement = "bottom", trigger = "hover", options = NULL)
          ),
          sliderInput("motion_blur", "Max. motion blur (px)", min = 0.01, max = 5, step = 0.01, value = default_max_motion_blur, width = "100%"),
          HTML("<b>Hint:</b> Try increasing shutter speed if the minimum motion blur value is too high.")
        ),
        
        tabPanel("GCPs",
          h4("Recover frequency"),
          fluidRow(
            column(6, numericInput("gcp_rec_frequency", "Frequency of images (%)", width = "100%", value = default_gcp_rec_frequency, step=1)),
            column(6, sliderInput("gcp_rec_number", "... with min. number of visible GCPs", width = "100%", value = default_gcp_rec_number, step=1, min=1, max=3))
          ),
          radioButtons(inline=TRUE, "gcp_frequency_base", "Frequency based on",
                       choiceNames = list(
                         p("Mapping area", width="100px"),
                         p("Flight area", width="100px")),
                       choiceValues = list(
                         "inline", "complete"
                       ),
                       selected="inline"),
          
          h4("Arrangement"),
          checkboxInput("edit_gcp_arrangement", "Set manually"),
          fluidRow(
            column(6, numericInput("gcp_n_in_x", "Number of GCPs, width", width = "100%", value = default_no_gcp_x, step=1)),
            column(6, numericInput("gcp_n_in_y", "Number of GCPs, depth", width = "100%", value = default_no_gcp_y, step=1))
          ),
          radioButtons(inline=TRUE, "gcp_arrangement_pattern", "Arrangement pattern",
                       choiceNames = list(
                         p("Squared", width="100px"),
                         p("Crosswise", width="100px")),
                       choiceValues = list(
                         "quad", "skip"
                       ),
                       selected="skip"),
          fluidRow(
            column(4, plotOutput("gcp_arrangement_quad", height = "50px", width = "100px")),
            column(4, plotOutput("gcp_arrangement_skip", height = "50px", width = "100px"))
          )
        ),
        
        tabPanel("Location",
                 h4("Mapping area"),
                 fluidRow(
                   column(6,numericInput("position_edge1_lat", "Mapping area edge (Latitude)", width = "100%", value = default_position_edge1_lat)),
                   column(6, numericInput("position_edge1_long","Mapping area edge (Longitude)", width = "100%", value = default_position_edge1_long))
                 ),
                 fluidRow(
                   column(6,numericInput("position_edge2_lat", "Flight direction (Latitude)", width = "100%", value = default_position_edge2_lat)),
                   column(6, numericInput("position_edge2_long","Flight direction (Longitude)", width = "100%", value = default_position_edge2_long))
                 ),
                 fluidRow(
                   column(6,numericInput("position_start_lat", "Start location (Latitude)", width = "100%", value = default_starting_point_lat)),
                   column(6, numericInput("position_start_long","Start location (Longitude)", width = "100%", value = default_starting_point_long))
                 ),
                 hr(),
                 numericInput("max_number_of_wp", "Maximum number of waypoints", width = "100%", value = default_max_number_of_wp),
                 bsTooltip("max_number_of_wp", "Maximum number of waypoints the drone is able to handle for way-point flights (e.g. 99 for DJI systems)", placement = "bottom", trigger = "hover", options = NULL)
                 
        )
      ),
      hr(),
      
      img(src="ETHLogo_black_20.png"),
      div(
        HTML("<br />Group of crop science ("),
        tags$a("http://www.kp.ethz.ch", href="http://www.kp.ethz.ch/infrastructure/uav-phenofly.html", target="_blank"),
        HTML(")"),
        HTML("<br />Lukas Roth ( lukas.roth@usys.ethz.ch )<br />"),
        HTML("(c) 2018 - "), tags$a("GPL-3.0", href="https://www.gnu.org/licenses", target="_blank"),
        HTML("<br />"),
        tags$a("https://gitlab.ethz.ch/crop_phenotyping/PhenoFlyPlanningTool", href="https://gitlab.ethz.ch/crop_phenotyping/PhenoFlyPlanningTool", target="_blank"),
        HTML("<br />"),
        HTML("<br />Based on publication:"), tags$a("Roth et al. (2018). PhenoFly Planning Tool: Flight planning for high-resolution optical remote sensing with unmanned areal systems. Plant Methods, 14(1).", href="https://rdcu.be/bd7qq", target="_blank")
    )),
    
    mainPanel(

      tabsetPanel(id="results",
        tabPanel("Photography",
          fluidRow(
            column(8, h4("Flight height dependency"), 
                   plotOutput("photographic_properties_plot", height = "600px")),
            column(4, htmlOutput("photographic_properties_summary"))
          )
        
        ),
        tabPanel("Mapping properties",
          fluidRow(
            column(8,
                   h4("Mapping area"),
                   tabsetPanel(type="pills",
                     tabPanel("Schematic", 
                              plotOutput("plot_mapping_area", height = "600px")
                      ),
                     tabPanel("Map", 
                      leafletOutput("waypoint_map", height = "600px"),
                      actionButton("recalc_waypoint_map", "Recalc waypoints"),
                      htmlOutput("waypoint_settings")),
                     tabPanel("GCP recover frequency", 
                              plotOutput("plot_hits_gcp", height = "600px"))
                   )
            ),
            column(4, htmlOutput("mapping_summary"))
          )
        ),
        tabPanel("Viewing geometry",
                 fluidRow(
                   column(6, h4("Sensor viewing geometry"),
                          tabsetPanel(type="pills",
                                      tabPanel("Sensor area", 
                                               plotOutput("plot_recovery_frequency", height = "500px")),
                                      tabPanel("Sensor X axis", 
                                              plotOutput("plot_viewing_geometry_x", height = "500px")),
                                      tabPanel("Sensor Y axis", 
                                               plotOutput("plot_viewing_geometry_y", height = "500px"))
                          )),
                   column(6, h4("Plot center viewing geometry"),
                          tabsetPanel(type="pills",
                                      tabPanel("Zenith angle", 
                                               plotOutput("plot_viewing_geometry", height = "500px")),
                                      tabPanel("Azimuth angle", 
                                               plotOutput("plot_viewing_geometry2", height = "500px"))
                          ))
                 )
           
        ),
        tabPanel("Mission briefing",
                 fluidRow(
                   column(5, 
                     h4("Camera settings"),
                     htmlOutput("camera_settings"),
                     h4("Flight/campaign settings"),
                     htmlOutput("campaign_settings"),
                     h4("Restrictions"),
                     htmlOutput("restrictions")
                   ),
                 column(7, 
                        leafletOutput("mission_waypoint_map", height=400) )
           ),
           hr(),
           h4("Waypoints"),
           div(downloadButton("download_waypoints", "Download waypoints as CSV"), HTML("(e.g. to import in Litchi)")),
           p(),
           div(downloadButton("download_kml", "Download mapping area as KML"), HTML("(e.g. to import in DJI GS Pro)")),
           hr(),
           h4("Report"),
           div(downloadButton("download_report", "Download all graphs as PDF"))
      ))
    )
  )
)
         